public class Draw {
    public static void main(String[] args) {
        drawSquare(4);
        drawTriangle(4);
    }

    public static void drawSquare(int size) {
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawTriangle(int size) {
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size - i - 1; j++) {
                System.out.print(" ");
            }
            for(int j = 0; j < 2 * i + 1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}