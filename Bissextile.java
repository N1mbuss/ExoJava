public class Bissextile {
        public static void main(String[] args) {
            int year = 2023;
            System.out.println(EstBissextile(year));
            printNext100Bissextile(year);
        }

        public static boolean EstBissextile(int year) {
            if(year % 4 != 0) {
                return false;
            } else if(year % 100 != 0) {
                return true;
            } else return year % 400 == 0;
        }

        public static void printNext100Bissextile(int year) {
            int count = 0;
            while (count < 100) {
                year++;
                if (EstBissextile(year)) {
                    System.out.println(year);
                    count++;
                }
            }
        }

}
