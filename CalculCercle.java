import java.util.Scanner;

public class CalculCercle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char choix = ' ';

        while (choix != 's' && choix != 'p') {
            System.out.println("Saisir 's' pour la surface et 'p' pour le périmètre : ");
            choix = scanner.next().charAt(0);

            if (choix != 's' && choix != 'p') {
                System.out.println("Veuillez saisir 's' ou 'p' !");
            }
        }

        if (choix == 's') {
            System.out.println("Entrez le rayon du cercle pour calculer la surface : ");
            double rayon = scanner.nextDouble();
            double surface = Math.PI * rayon * rayon;
            System.out.printf("La surface du cercle est : %.3f", surface);
        } else if (choix == 'p') {
            System.out.println("Entrez le rayon du cercle pour calculer le périmètre : ");
            double rayon = scanner.nextDouble();
            double perimetre = 2 * Math.PI * rayon;
            System.out.printf("Le périmètre du cercle est : %.3f ", perimetre);
        }

        scanner.close();
    }
}