import java.util.Scanner;

/**
 * Classe principale pour vérifier si une année est bissextile.
 */
public class VerifBissextile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez saisir une année :");
        int year = scanner.nextInt();
        if (verif(year)) {
            System.out.println("L'année " + year + " est bissextile.");
        } else {
            System.out.println("L'année " + year + " n'est pas bissextile.");
        }
    }

    /**
     * Méthode pour vérifier si une année est bissextile.
     *
     * @param year L'année à vérifier.
     * @return true si l'année est bissextile, false sinon.
     */
    public static boolean verif(int year) {
        if (year % 4 != 0) {
            return false;
        } else if (year % 100 != 0) {
            return true;
        } else return year % 400 == 0;
    }
}