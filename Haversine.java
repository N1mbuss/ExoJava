public class Haversine {
    public static void main(String[] args) {
        double lat1 = 51.5074, lon1 = 0.1278; // Londres
        double lat2 = 48.8566, lon2 = 2.3522; // Paris
        System.out.println(haversine(lat1, lon1, lat2, lon2) + " km");
    }

    /***
     *
     * @param lat1 : latitude ville 1
     * @param lon1 : longitude ville 1
     * @param lat2 : latitude ville 2
     * @param lon2 : longitude ville 2
     * @return : Distance en km entre les 2
     */
    public static double haversine(double lat1, double lon1, double lat2, double lon2) {
        final int R = 6371; // Radius de la terre en km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c; // Distance en km
    }
}
